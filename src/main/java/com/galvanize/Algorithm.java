package com.galvanize;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Algorithm {

    public boolean allEqual(String str) {
        if (str.length() == 0) return false;

        char first = str.charAt(0);
        for(char c: str.toCharArray()) {
            if (Character.toLowerCase(c) != Character.toLowerCase(first)) {
                return false;
            }
        }
        return true;
    }

    public Map<String, Long> letterCount(String str) {

        Map<String, Long> count = new HashMap<>();

        for (char c: str.toCharArray()) {
            char lowercase = Character.toLowerCase(c);
            long val = count.containsKey(Character.toString(lowercase)) ? count.get(Character.toString(lowercase)) : 0;
            count.put(Character.toString(lowercase), val + 1);
        }
        return count;
        
    }

    public String interleave(List<String> lst1, List<String> lst2) {
        StringBuilder sb = new StringBuilder();

        if (lst1.isEmpty()) {
            return "";
        }

        for (int i = 0; i < lst1.size(); i++) {
            sb.append(lst1.get(i));
            sb.append(lst2.get(i));
        }
        return sb.toString();
    }

}
