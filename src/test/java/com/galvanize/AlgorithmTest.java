package com.galvanize;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class AlgorithmTest {
    private Algorithm algo;

    @BeforeEach
    void setUp() {
        algo = new Algorithm();
    }

    @Test
    void equalsAllBs() {
        boolean actual = algo.allEqual("aaaAa");

        assertTrue(actual);
    }

    @Test
    void emptyStringEqualsAll() {
        boolean actual = algo.allEqual("");

        assertFalse(actual);
    }


    @Test
    void correctLetterCount() {
        Map<String, Long> actual = algo.letterCount("abc");

        Map<String, Long> expected = new HashMap<>();
        expected.put("a", 1L);
        expected.put("b", 1L);
        expected.put("c", 1L);


        assertEquals(expected, actual);
    }

    @Test
    void interleaveTwoLists() {
        String actual = algo.interleave(Arrays.asList("a", "c", "e"),Arrays.asList("b", "d", "f"));

        String expected = "abcdef";

        assertEquals(expected, actual);

    }
}
